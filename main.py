#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  main.py
#  
#  Copyright 2020 Strannik-j <strannik-j@mail.ru>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import argparse
import pandas as pd
import logging.config
from my_config import *
from random import choice
from threading import Thread
from you_parser import *
from youtube_api import Youtuber
from glob import glob
from shutil import rmtree, copytree, Error
from time import sleep
from queue import Queue
from os import system
from os.path import sep
from pathlib import Path
from sys import exc_info
from my_decorators import print_def_name
from my_loggers import loggers_config


logging.config.dictConfig(loggers_config)
log_main = logging.getLogger('log_main')
log_null = logging.getLogger('log_null')

HOME_DIR = expanduser('~')


@print_def_name
def read_channels_list(Path):
    channels_df = pd.read_csv(Path, header=0,sep=";", encoding='UTF-8').dropna(axis=1, how='all').dropna(axis=0, how='any')
    return channels_df


@print_def_name
def get_video_id(url):
    video_id = url
    video_id = video_id.replace('https','')
    video_id = video_id.replace('http','')
    video_id = video_id.replace('://','')
    video_id = video_id.replace('www.','')
    video_id = video_id.replace('m.youtube.com/','')
    video_id = video_id.replace('youtube.com/','')
    video_id = video_id.replace('youtu.be/','')
    video_id = video_id.replace('watch?v=','')
    # if '?list=' in video_id:
        # video_id = video_id[:video_id.index('list=')-1]
    if '&' in video_id:
        video_id = video_id[:video_id.index('&')]
    if '?' in video_id:
        video_id = video_id[:video_id.index('?')]
    return video_id


@print_def_name
def ReadVideosList(Path):
    with open(Path, 'r') as f:
        res = f.read().splitlines()
    res1 = []
    for i in res:
        res1.append(get_video_id(i))
    return res1


@print_def_name
def get_videos_list(YT):
    global max_videos
    global random
    global chan_list
    global vid_list
    videos_list = []
    if chan_list:
        channels_df = read_channels_list('channels_list.csv')

        videos = {}
        for i, r in channels_df.iterrows():
            try:
                videos.update(YT.get_channel_videos(r['ID']))
            except TypeError as err:
                log_main.error(f'Канал недоступен: {r["Name"]}  -  {r["ID"]}')

        channels = list(videos.keys())
        while True:
            try:
                chan = choice(channels)
                channels.remove(chan)
                
                for i in range(0, max_videos):
                        if random:
                            video = choice(videos[chan])
                        else:
                            video = videos[chan][0]
                        
                        videos[chan].remove(video)
                        videos_list.append(video)

            except IndexError:
                break
    if vid_list:
        videos_list += ReadVideosList('videos_list.txt')
        
    videos_list_randomized = []
    for i in range(0, len(videos_list)):
        videos_list_randomized.append(videos_list.pop(videos_list.index(choice(videos_list))))
    return videos_list_randomized
    

class MyThread(Thread):
    def __init__(self, Name, queue, YT, volume, speed, browser, profile, hidden, legacy, rating):
        Thread.__init__(self)
        self.name = Name
        # self.VideosList = VideosList
        self.YT = YT
        self.volume = volume
        self.speed = speed
        self.browser = browser
        self.profile = profile
        self.hidden = hidden
        self.legacy = legacy
        self.queue = queue
        self.status = None
        self.exit = False
        self.rating = rating
        log_main.info(f'{self.name}: Создан поток')
    
    def __del__(self):
        self.delete_YP()

        
    @print_def_name
    def create_YP(self):
        # Создание нового объекта    
        self.YP = YoutubeParser(volume=self.volume, speed=self.speed, browser=self.browser, profile= self.profile, hidden=self.hidden, legacy=self.legacy, thread=self.name)
        log_main.info(f'{self.name}: Создан парсер')
        # return self.YP
    
    
    @print_def_name
    def clean_system(self):
        # Чистка диска
        log_main.debug(f'{self.name}: Чистка диска')
        try:
            system('rm -rf /tmp/rust_mozprofile*')

        except Exception as err:
            # log_main.error(f'{self.name}: RM /tmp/rust_mozprofile* err: {err.__class__}: {err}')
            pass

        try:
            system('rm -rf geckodriver*')

        except Exception as err:
            # log_main.error(f'{self.name}: RM geckodriver* err: {err.__class__}: {err}')
            pass


    @print_def_name
    def delete_YP(self):
        try:
            self.YP.driver.close()
            log_main.error(f'{self.name}: Закрываю окно')
            
            del self.YP
            log_main.info(f'{self.name}: Уничтожен объект')

        except Exception as err:
            log_main.debug(f'{self.name}: EX ERR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err} Неудачная попытка закрыть окно')
        
        finally:
            self.clean_system()
        return None


    @print_def_name
    def run(self):
        ch = 0
        self.YP = None
        video_id = None
        while True:
            try:
                if not video_id:
                    video_id = self.queue.get()
                self.status = True
                ch +=1
                
                if ch % 10 == 0:
                    log_main.info(f'{self.name}: Штатный перезапуск окна ({ch})')
                    self.YP = self.delete_YP()
                        
                if self.YP is None:
                    self.clean_system()
                    self.create_YP()
                    
                res = self.YP.watch_video(video_id, self.rating)
                if str(res) == 'exit':
                    self.YP = self.delete_YP()
                    self.exit = True
                    exit()

                video_id = None
                self.status = False
            except Exception as err:
                log_main.debug(f'{self.name}: Some ERROR:\nLine: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                self.delete_YP()
                sleep(10)


@print_def_name
def remove_profiles(profile_path):
    profile_path = profile_path.strip()
    
    while profile_path[-1] in ('/', '\\'):
        profile_path = profile_path[:-1]
    
    # Очистка веменных профилей
    while True:
        prof_dirs = glob(f'{profile_path}-*')
        log_main.debug(f'To remove prof_dirs={prof_dirs}')
        if len(prof_dirs) > 0:
            try:
                rmtree(prof_dirs[-1])
            except Error as err:
                log_main.error(f'Remove profiles ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                exit()
        else:
            break

@print_def_name
def create_profiles(profile_path, Threads):
    profile_path = profile_path.strip()
    
    while profile_path[-1] in ('/', '\\'):
        profile_path = profile_path[:-1]

    # profiles_dir, profile_name = split(profile_path)
    log_main.debug(f'Profile path: {profile_path}')
    
    remove_profiles(profile_path)
    
    # Создание временных профилей
    for i in range(0, Threads):
        while True:
            try:
                copytree(profile_path, f'{profile_path}-{i + 1}')
                break
            except Error as err:
                log_main.error(f'Copy profiles ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                log_main.error(f'May be program have not permissions to do this. Exit.')
                try:
                    p = Path(f'{profile_path}{sep}lock')
                    p.unlink()
                except Exception as err:
                    log_main.error(f'Some pathlib ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                    raise err

            except FileExistsError as err:
                log_main.error(f'I can not copy profiles. I must remove exists profiles: FileExistsError: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                remove_profiles(profile_path)
            except Exception as err:
                log_main.error(f'Copy profiles some ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                log_main.exception(err)

    ProfDirs = glob(f'{profile_path}-*')
    return ProfDirs
    

@print_def_name
def run(ProfilesList):
    global volume
    global browser
    global speed
    global hidden
    global legacy
    global random
    global max_videos
    global threads
    global chan_list
    global vid_list
    global rating

    log_main.info(f'hidden = {hidden}')
    log_main.info(f'volume = {volume}')
    log_main.info(f'speed = {speed}')
    log_main.info(f'browser = {browser}')
    log_main.info(f'legacy = {legacy}')
    log_main.info(f'random = {random}')
    log_main.info(f'max_videos = {max_videos}')
    log_main.info(f'threads = {threads}')
    log_main.info(f'chan_list = {chan_list}')
    log_main.info(f'vid_list = {vid_list}')
    log_main.info(f'rating = {rating}')

    main_queue = Queue()
    YT = Youtuber()
    # VideosLists = get_videos_list(YT, threads)
    videos_list = get_videos_list(YT)
    log_main.debug(f'videos_list: \n{videos_list}')
    if len(videos_list) < threads:
        threads = len(videos_list)
        pass
    my_threads = []
    
    for i in range(0, threads):
        thread_name = f'Thread #{i+1}'
        my_thread = MyThread(thread_name, main_queue, YT, volume, speed, browser, ProfilesList[i], hidden, legacy, rating)
        my_thread.setDaemon(True)
        my_thread.start()
        my_threads.append(my_thread)
        
    while True:
        log_main.info(f'Загрузка очереди')
        for video_id in videos_list:
            main_queue.put(video_id)
        while True:
            all_status = False
            for thread in my_threads:
                if thread.exit:
                    exit()
                if thread.status in (None, True):
                    all_status = True
            if all_status == False:
                break
            sleep(1)
        log_main.info(f'Список закончился. Создаём новый.')
        videos_list = get_videos_list(YT)
    

@print_def_name
def BOOL(S):
    if str(S).lower() in ('1', 'true', 't'):
        return True
    elif str(S).lower() in ('0', 'false', 'f'):
        return False
    elif str(S).lower() in ('none'):
        return None


@print_def_name
def get_args(args):
    global volume
    global browser
    global speed
    global hidden
    global legacy
    global random
    global max_videos
    global threads
    global rating

    
    if args.hidden:
        hidden = BOOL(args.hidden)
    if args.volume:
        volume = args.volume
    if args.speed:
        speed = args.speed
    if args.browser:
        browser = args.browser
    if args.legacy:
        legacy = BOOL(args.legacy)
    if args.random:
        random = BOOL(args.legacy)
    if args.max_videos:
        max_videos = args.legacy
    if args.threads:
        threads = args.threads
    if args.rating:
        rating = args.rating


    
    

@print_def_name
def main(args):
    global random
    global browser
    global threads
    global firefox_profile
    global HOME_DIR

    log_null.info('')
    log_main.info('Запуск программы')

    parser = argparse.ArgumentParser('Process some integers.')
    parser.add_argument('-H', '--hidden', type=str, help='hidden')
    parser.add_argument('-V', '--volume', type=str, help='volume')
    parser.add_argument('-S', '--speed', type=float, help='speed')
    parser.add_argument('-B', '--browser', type=str, help='browser')
    parser.add_argument('-L', '--legacy', type=str, help='legacy')
    parser.add_argument('-R', '--random', type=str, help='random')
    parser.add_argument('-M', '--max_videos', type=int, help='max_videos')
    parser.add_argument('-T', '--threads', type=int, help='threads')
    parser.add_argument('-A', '--rating', type=str, help='rating')
    args = parser.parse_args()
    get_args(args)
    
    if browser == 'Firefox':
        if HOME_DIR not in firefox_profile:
            firefox_profile = join(HOME_DIR, firefox_profile.strip())
        profiles_list = create_profiles(firefox_profile, threads)
        log_main.debug(f'profiles_list={profiles_list}')
        
    else:
        log_main.error(f'Браузер {browser} пока не поддерживается. Выход.')
        exit()
    try:
        while True:
            run(profiles_list)
            if random:
                random = False
            else:
                random = True
    
    except KeyboardInterrupt:
        log_main.info('Принудительное завершение')
        log_null.info('')
    except Exception as err:
        log_main.error(err)
        raise 
    finally:
        remove_profiles(firefox_profile)
        
if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))

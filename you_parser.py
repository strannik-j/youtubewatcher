#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  you_parser.py
#
#  Copyright 2017 Strannik-j <strannik-j@mail.ru>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import logging.config
from my_exceptions import ExitNow
from os.path import expanduser, join
from selenium.common.exceptions import JavascriptException
from os import system
from sys import exc_info
from my_decorators import print_def_name
from my_loggers import loggers_config
from selenium import webdriver
from time import strftime
from time import sleep
from os import getcwd

logging.config.dictConfig(loggers_config)
log_parser = logging.getLogger('log_parser')
log_null = logging.getLogger('log_null')


pwd = getcwd()
Date = strftime('%Y-%m-%d_%H%M%S')
#TimeZone = '+3'


class YoutubeParser(object):
    def __init__(self, link=None, volume='0.0', speed=2, browser='Firefox', profile=None, hidden = False, legacy=False, thread=1):
        self.home_dir = expanduser('~')
        self.link = link
        self.volume = volume
        self.speed = speed
        self.browser = browser
        self.hidden = hidden
        self.legacy = legacy
        self.thread = thread
        self.profile = profile

        if self.browser == "Firefox":
            log_parser.info(f'{self.thread}: Запуск Firefox')
            fp = webdriver.FirefoxProfile(join(self.home_dir, self.profile))
            fp.set_preference("media.volume_scale", self.volume)
            options = webdriver.FirefoxOptions()
            if self.legacy:
                options.set_headless(headless=self.hidden)
            else:
                options.headless = self.hidden
            self.driver = webdriver.Firefox(fp, options=options)
            log_parser.debug(f'{self.thread}: Profiler')

        elif self.browser == "Chrome":
            log_parser.info(f'{self.thread}: Chrome')
            user_data_path = '/home/strannik/.config/chromium/profile 1'
            # ChromeOptions = webdriver.ChromeOptions().add_argument("user-data-dir=" + user_data_path)
            ChromeOptions = webdriver.ChromeOptions().add_argument("user-data-dir=/home/strannik/.config/chromium/Profile_1")
            # self.driver = webdriver.Chrome(chrome_options=ChromeOptions)
            self.driver = webdriver.Chrome(options=ChromeOptions)
            log_parser.debug(f'{self.thread}: Profiler')
   
        else:
            log_parser.error(f'{self.thread}: No Browser')


    @print_def_name
    def get_rating(self):
        try:
            sleep(1)
            like_btn = self.driver.find_element_by_xpath(
                "//div[@id='info']//ytd-toggle-button-renderer[1]//a[1]//yt-icon-button[1]//button[1]")
            diss_btn = self.driver.find_element_by_xpath(
                "//div[@id='info']//ytd-toggle-button-renderer[2]//a[1]//yt-icon-button[1]//button[1]")

            if like_btn.get_attribute('aria-pressed') == 'true':
                return 'like'
            elif diss_btn.get_attribute('aria-pressed') == 'true':
                return 'dislike'
            else:
                return 'none'
        except Exception as err:
            log_parser.info(f'{self.thread}: get_rating ERROR:\nLine: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')


    @print_def_name
    def set_rating(self, rating):
        rating = rating.lower()
        cur_rating = self.get_rating()
        log_parser.debug(f'{self.thread}: cur_rating: {cur_rating}')
        
        sleep(1)
        like_btn = self.driver.find_element_by_xpath("//div[@id='info']//ytd-toggle-button-renderer[1]//a[1]//yt-icon-button[1]//button[1]")
        diss_btn = self.driver.find_element_by_xpath("//div[@id='info']//ytd-toggle-button-renderer[2]//a[1]//yt-icon-button[1]//button[1]")
            
        # if cur_rating == rating:  # Оригинальное условие
        if cur_rating != 'none':    # Небольшое изменение, чтобы программа не меняла рейтинг, установленный вручную
            return True             # TODO: Возможно, стоит вынести этот пункт в фаил конфигурации
        log_parser.debug(f'{self.thread}: set_rating: {rating}')
        if rating == 'like':
            like_btn.click()
        elif rating == 'dislike':
            diss_btn.click()
        elif rating == 'none':
            if cur_rating == 'like':
                like_btn.click()
            elif cur_rating == 'dislike':
                diss_btn.click()
        elif rating == 'disable':
            return 0
        else:
            log_parser.error(f'Error in my_config.py. Rating "{rating}" is not walid. Use "like", "dislike", "none" or "disable".')


    @print_def_name
    def watch_video(self, video_id, rating=None):
        video_id = video_id.replace('https', '')
        video_id = video_id.replace('http', '')
        video_id = video_id.replace('://', '')
        video_id = video_id.replace('www.', '')
        video_id = video_id.replace('m.youtube.com/', '')
        video_id = video_id.replace('youtube.com/', '')
        video_id = video_id.replace('youtu.be/', '')
        video_id = video_id.replace('watch?v=', '')
        if '&' in video_id:
            video_id = video_id[:video_id.index('&')]
        if '?' in video_id:
            video_id = video_id[:video_id.index('?')]
        link = f'https://www.youtube.com/watch?v={video_id}'
        
        if 'autoplay=' not in link:
            link += '&autoplay=1'
        if 'enablejsapi=' not in link:
            link += '&enablejsapi=1'
        if 'start=' not in link:
            link += '&start=0'
        if 'rel=' not in link:
            link += '&rel=0'
        
        while True:
            try:
                self.driver.get(link)
                try:
                    player_speed = self.driver.execute_script("return document.getElementById('movie_player').getPlaybackRate()")
                    if player_speed != self.speed:
                        self.driver.execute_script(f"return document.getElementById('movie_player').setPlaybackRate({self.speed})")
                    break
                except Exception as err:
                    log_parser.error(f'{self.thread}: Parser ERROR: Line: {sys.exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                    sleep(3)
                    try:
                        if 'https://www.google.com/recaptcha/api.js' in self.driver.page_source:
                            log_parser.error(f'{self.thread}: reCAPTCHA! Exit')
                            return 'exit'
                        else:
                            log_parser.debug(f'{self.thread}: Recaptha source: {self.driver.page_source}')
                    except Exception as err:

                        log_parser.debug(f'{self.thread}: reCAPTCHA ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                        return 'exit'

            except TimeoutError as err:
                log_parser.debug(f'{self.thread}: TimeoutError: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')

        while True:
            try:
                video_name = self.driver.find_element_by_xpath('''/html[1]/body[1]/ytd-app[1]/div[1]/ytd-page-manager[1]/ytd-watch-flexy[1]/div[4]/div[1]/div[1]/div[5]/div[2]/ytd-video-primary-info-renderer[1]/div[1]/h1[1]/yt-formatted-string[1]''').text
                channel_name = self.driver.find_element_by_xpath('''/html[1]/body[1]/ytd-app[1]/div[1]/ytd-page-manager[1]/ytd-watch-flexy[1]/div[4]/div[1]/div[1]/div[6]/div[3]/ytd-video-secondary-info-renderer[1]/div[1]/div[2]/ytd-video-owner-renderer[1]/div[1]/ytd-channel-name[1]/div[1]/div[1]/yt-formatted-string[1]/a[1]''').text
                break
            except Exception as err:
                log_parser.debug(f'Find video_name & channel_name ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                sleep(1)
        log_null.info('')
        log_parser.info(f'{self.thread}: video_url: {link}')
        log_parser.info(f'{self.thread}: Канал: "{channel_name}"')
        log_parser.info(f'{self.thread}: Видео: "{video_name}"')
        ads_status = None
        cur_player_status = None
        pause_timer = 10
        try:
            while True:
                player_status = self.driver.execute_script("return document.getElementById('movie_player').getPlayerState()")
                # log_parser.debug(f'{self.thread}: player_status={player_status}')
                if player_status != cur_player_status:
                    cur_player_status = player_status
                    log_parser.debug(f'player_status={player_status}')
                # integer
                # -1: воспроизведение видео не началось или реклама
                # 0: воспроизведение окончено
                # 1: ролик воспроизводится
                # 2: пауза
                # 3: буферизация
                # 5: видео находится в очереди
                if player_status == -1:
                    if not ads_status:
                        log_parser.info(f'{self.thread}: Началась реклама')
                        ads_status = 1
                else:
                    if ads_status == 1:
                        log_parser.info(f'{self.thread}: Закончилась реклама')
                        ads_status = None

                if player_status == 0:
                    if rating:
                        self.set_rating(rating)
                    return 0
                # Костыль от застревания видео в очереди
                # TODO: написать автоматическое продолжение проигрывания
                elif player_status in (5,2):
                    if pause_timer == 0:
                        return 0
                    else:
                        pause_timer -= 1
                # sleep(0.1)
                sleep(1)

        except JavascriptException as err:
            log_parser.error(f'{self.thread}: JavascriptException: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            # return -5
        
    def run(self):
        self.driver.implicitly_wait(2)
        self.watch_video(self.link)


def main(args):
    thread = 1
    log_parser.info(f'{thread}: Запуск парсера')

    while True:
        try:
            YP = YoutubeParser()
            log_parser.info(f'{thread}: Создан парсер')
            res = YP.watch_video('http://www.youtube.com/watch?v=XQ7FMbMLFxc')
            if res == 0:
                YP.driver.close()
                del YP
                
        except ExitNow:
            exit()

        except Exception as err:
            log_parser.error(f'{thread}: Some ERROR:\n Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')

            try:
                YP.driver.close()
                log_parser.error(f'{thread}: Закрываю окно')

            except OSError as err:
                log_parser.error(f'{thread}: OSError: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                

                # Чистка диска
                try:
                    system('rm -rf /tmp/')
                    
                except Exception as err:
                    log_parser.error(f'{thread}: RM tmp err: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')

                try:
                    system('rm -rf geckodriver*')
                    
                except Exception as err:
                    log_parser.error(f'{thread}: RM geckodriver* err: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')

            except Exception as err:
                log_parser.error(f'{thread}: EX ERR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err} Неудачная попытка закрыть окно')
                
            log_parser.error(f'{thread}: Перезагрузка парсера')
    
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))



#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  my_config.py
#  
#  Copyright 2020 Strannik-j <strannik-j@mail.ru>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

max_videos = 10
random = True

# hidden = False
hidden = True

browser= "Firefox"
legacy = False
# browser="Chrome" # NOT SUPPORTED NOW

speed = 2
volume = '0.0'

threads = 5

chan_list = True
vid_list = False
rating = 'like'  # Can be 'like', 'dislike', 'none' or 'disable'
firefox_profile = '.mozilla/firefox/some.profile'

# My public key for this project. You can change this if you want.
YOUTUBE_KEY = "AIzaSyBce3GwMei_xFjm9Hyt3e4sD1hDWBQx4AM"
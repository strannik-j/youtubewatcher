# YoutubeWatcher

Let the computer watch youtube instead of you.

## How to use:

### Ubuntu:
Create a special profile in the Firefox:  
`firefox --ProfileManager`  
Start it, and login into your google account. Besides you can setup proxy.  
Close the Firefox.  
Find the path to the profile and write it to a my_config.py file:  
`firefox_profile` = '.mozilla/firefox/some_profile'  
  
Then configure the program:
```
sudo apt update  
sudo apt install git python3-venv firefox-geckodriver  
  
git clone https://gitlab.com/strannik-j/youtubewatcher/  
cd youtubewatcher  
  
python3 -m venv ~/.youtubewatcher_env  
source ~/.youtubewatcher_env/bin/activate  
pip3 install -r requirements.txt  
  
python3 main.py -T1  
```

  
*parameters*:  
-H --hidden:  Hidden mode. Dont show browser window. 0 - no, 1 - yes.  (Default:  1)  
-V --volume:  Volume 1 - normal, 0 - mute.  (Default:  0)  
-S --speed:  Playback speed.  (Default:  2)  
-B --browser:  Browser.  (Default:  Frefox)  
-L --legacy:  Legacy mode. 0 - no, 1 - yes.  (Default:  0)  
-R --random:  Randomly playing videos from a playlist. 0 - no, 1 - yes.  (Default:  1)  
-M --max_videos:  Max videos before cleaning.  (Default:  10)  
-T --threads:  Threads.  (Default:  1)  
-A --rating:  Set rating ('like', 'dislike'), unset any rating ('none') and 'disable'.


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  youtube_api.py
#
#  Copyright 2020 Strannik-j <strannik-j@mail.ru>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import httplib2
import os
import logging.config
from sys import exc_info
from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow
from my_decorators import print_def_name
from my_loggers import loggers_config
from my_config import YOUTUBE_KEY

logging.config.dictConfig(loggers_config)
log_api = logging.getLogger('log_api')
log_null = logging.getLogger('log_null')


you_list = (
    'youtube.',
    'youtu.be/'
)


class ListFilter():
    def __init__(self, L1, L2):
        self.L1 = L1
        self.result = list(filter(self.fil, L2))

    def fil(self, L2):
        for i in self.L1:
            if i in L2:
                return True


class Youtuber():
    def __init__(self, Text=None, VideoLink = None, VideoID=None, UserID=None):
        log_api.info('Creating Youtuber object')
        
        self.YOUTUBE_READ_WRITE_SCOPE = "https://www.googleapis.com/auth/youtube"
        self.YOUTUBE_API_SERVICE_NAME = "youtube"
        self.YOUTUBE_API_VERSION = "v3"
        self.YOUTUBE_KEY= YOUTUBE_KEY
        self.CLIENT_SECRETS_FILE = "client_secret.json"
        self.MISSING_CLIENT_SECRETS_MESSAGE = """
        WARNING: Please configure OAuth 2.0

        To make this sample run you will need to populate the client_secrets.json file
        found at:

             %s

        with information from the {{ Cloud Console }}
        {{ https://cloud.google.com/console }}

        For more information about the client_secrets.json file format, please visit:
        https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
        """ % os.path.abspath(os.path.join(os.path.dirname(__file__),
                                            self.CLIENT_SECRETS_FILE))
        
        self.user_id = UserID
        self.video_link = VideoLink
        self.video_id = VideoID
        # self.auth = self.get_auth()
        self.auth = self.get_public_auth()
        self.text = Text
        
        # self.run()
        
    @print_def_name
    def run(self):
        if self.text:
            if not self.video_link:
                log_api.debug(f'\n\nself.text={self.text}' )
                self.get_video_link()
                log_api.debug(f'self.video_link={self.video_link}')
            if not self.video_id:
                self.get_video_id()
                log_api.debug(f'self.video_id={self.video_id}')
        

    @print_def_name
    def get_video_link(self):
        T = self.text.strip()
        L = []
        for x in T.split('\n'):
            for y in x.split(' '):
                L.append(y)
        LF = ListFilter(you_list, L)
        self.video_link = LF.result[0]


    @print_def_name
    def get_video_id(self):
        T = self.video_link
        T = T.replace('https','')
        T = T.replace('http','')
        T = T.replace('://','')
        T = T.replace('www.','')
        T = T.replace('m.youtube.com/','')
        T = T.replace('youtube.com/','')
        T = T.replace('youtu.be/','')
        T = T.replace('watch?v=','')
        # if '?list=' in T:
            # T = T[:T.index('list=')-1]
        if '&' in T:
            T = T[:T.index('&')]
        if '?' in T:
            T = T[:T.index('?')]
        self.video_id = T


    @print_def_name
    def clean_video(self):
        self.video_link = None
        self.video_id = None
        self.text = None


    @print_def_name
    def get_public_auth(self):
        youtube = build(
            self.YOUTUBE_API_SERVICE_NAME,
            self.YOUTUBE_API_VERSION,
            developerKey=self.YOUTUBE_KEY)
        return youtube


    @print_def_name
    def get_auth(self, args=None):
        flow = flow_from_clientsecrets(self.CLIENT_SECRETS_FILE,
            scope=self.YOUTUBE_READ_WRITE_SCOPE,
            message=self.MISSING_CLIENT_SECRETS_MESSAGE)

        oauth2_file_path = f"oauth2_{self.user_id}.json"
        if not os.path.exists(oauth2_file_path):
            log_api.debug('NOT AUTH')
            oauth2_file_path = "oauth2.json"
            if not os.path.exists(oauth2_file_path):
                raise FileNotFoundError
                pass
        log_api.debug(f'oauth2_file_path={oauth2_file_path}')
        storage = Storage(oauth2_file_path)
        credentials = storage.get()

        if credentials is None or credentials.invalid:
            credentials = run_flow(flow, storage, args)
            
            with open(oauth2_file_path, 'wt') as f:
                f.write(credentials.to_json())
            
        return build(self.YOUTUBE_API_SERVICE_NAME, self.YOUTUBE_API_VERSION,
            http=credentials.authorize(httplib2.Http()))


    @print_def_name
    def set_rating(self, Rating='none', ID=None):
        while True:
            try:
                if not ID:
                    ID = self.video_id
                res = 'None'
                try:
                    self.auth.videos().rate(
                        id=ID,
                        rating=Rating
                    ).execute()
                except HttpError as e:
                    log_api.debug("An HTTP error %d occurred:\n%s" % (e.resp.status, e.content))
                    res="An HTTP error %d occurred:\n%s" % (e.resp.status, e.content)
                else:
                    log_api.debug('video video_id="{}" set rating "{}".'.format(self.video_id, Rating))
                    res='video video_id="{}" set rating "{}".'.format(self.video_id, Rating)
                return res
            except TimeoutError:
                pass
            except Exception as err:
                log_api.debug(f'Some ERROR in set_rating:\nLine: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                break


    @print_def_name
    def get_rating(self, ID=None):
        while True:
            try:
                if not ID:
                    ID = self.video_id
                return self.auth.videos().getRating(id=ID).execute()['items'][0]['rating']
            except TimeoutError:
                pass
            except Exception as err:
                log_api.debug(f'Some ERROR in get_rating:\nLine: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                break


    @print_def_name
    def get_statistic(self, ID=None):
        if not ID:
            ID = self.video_id
        return self.auth.videos().list(part='snippet,contentDetails,statistics', id=ID).execute()['items'][0]['statistics']
    

    @print_def_name
    def get_descriptoin(self, ID=None):
        if not ID:
            ID = self.video_id
        response = self.auth.videos().list(part='snippet,contentDetails,statistics', id=ID).execute()
        log_api.debug(f'get_descriptoin: response={response}')
        description = response['items'][0]['snippet']['description']
        return description


    @print_def_name
    def get_channel_videos(self, URL):
        video_list = {}
        channel_id = URL
        channel_id = channel_id.replace('https','')
        channel_id = channel_id.replace('http','')
        channel_id = channel_id.replace('://','')
        channel_id = channel_id.replace('www.','')
        channel_id = channel_id.replace('m.youtube.com/','')
        channel_id = channel_id.replace('youtube.com/','')
        channel_id = channel_id.replace('youtu.be/','')
        channel_id = channel_id.replace('/videos', '')
        
        if '&' in channel_id:
            channel_id = channel_id[:channel_id.index('&')]
        if '?' in channel_id:
            channel_id = channel_id[:channel_id.index('?')]
        
        if 'channel/' in channel_id:
            while True:
                try:
                    channel_id = channel_id.replace('channel/', '')
                    log_api.debug(f'Channel: {channel_id}')

                    response = self.auth.channels().list(
                        part="snippet,contentDetails,statistics",
                        id=channel_id
                    ).execute()
                    log_api.debug(f'channel response={response}')
                    break
                except Exception as err:
                    log_api.error(f'channel response ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                    return False
        elif 'user/' in channel_id:
            channel_id = channel_id.replace('user/', '')

            log_api.debug(f'User={channel_id}')

            while True:
                try:
                    log_api.debug(f'get responce: channel_id={channel_id}')
                    response = self.auth.channels().list(
                        part="snippet,contentDetails,statistics",
                        forUsername=channel_id
                    ).execute()
                    log_api.debug(f'user response={response}')
                    break
                except Exception as err:
                    log_api.error(f'user response ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
                    return False
        else:
            return False

        try:
            channel_name = response['items'][0]['snippet']['title']
        except KeyError as err:
            log_api.debug(f'channel_name ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            return False
        except IndexError as err:
            log_api.debug(f'channel_name ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            return False
        except Exception as err:
            log_api.error(f'channel_name ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            return False

        video_list[channel_name] = []
        log_api.debug(f"get_channel_videos: channel_name={channel_name}")

        try:
            play_list_id = response['items'][0]['contentDetails']['relatedPlaylists']['uploads']
        except KeyError as err:
            log_api.debug(f'play_list_id ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            return False
        except IndexError as err:
            log_api.debug(f'play_list_id ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            return False
        except Exception as err:
            log_api.error(f'play_list_id ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            return False

        # Get Videos from channel
        try:
            result = self.auth.playlistItems().list(
                part="contentDetails",
                maxResults=25,
                playlistId=play_list_id
            ).execute()
        except Exception as err:
            log_api.error(f'Get videos from channel ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            return False

        for i in result['items']:
            video_list[channel_name].append(i['contentDetails']['videoId'])
        log_api.debug(f'video_list={video_list}')

        return(video_list)


if __name__ == "__main__":
    YT = Youtuber()
    videos_list = YT.get_channel_videos('https://www.youtube.com/channel/UCr3pz_CXjF_tURtZsD6itig')
    log_api.info(f'videos_list={videos_list}')
    log_api.info('Run with "main.py" file')
